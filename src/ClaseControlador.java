/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author franc
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.print.attribute.standard.MediaSize;

public class ClaseControlador implements ActionListener {

    ClaseModelo objModelo = null;
    ClaseVista objVista = null;

    public ClaseControlador(ClaseVista objVista, ClaseModelo objModelo) {

        this.objModelo = objModelo;
        this.objVista = objVista;
        actionListener(this); //ESCUCHADOR PARA EL BOTON
    }

    @Override
    public void actionPerformed(ActionEvent objEvento) {
        try {
            String numero1 = this.objVista.cajaDeTextoNumero1.getText();
            String numero2 = this.objVista.cajaDeTextoNumero2.getText();

            int resultado = objModelo.sumarDosEnteros(numero1, numero2);

            objVista.cajaDeTextoResultado.setText("" + resultado);

        } catch (Exception objException) {
            objException.printStackTrace();

        }
    }

    public void actionListener(ActionListener escuchador) {
        objVista.botonSumar.addActionListener(escuchador);
        objVista.botonRestar.addActionListener(escuchador);
    }

}
