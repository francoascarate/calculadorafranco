/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author franc
 */
public class ClaseModelo {

    public int sumarDosEnteros(String n1, String n2) {
        int numero1 = Integer.parseInt(n1);
        int numero2 = Integer.parseInt(n2);
        int resultado = numero1 + numero2;
        return (resultado);

    }

    public int restarDosEnteros(String n1, String n2) {
        int numero1 = Integer.parseInt(n1);
        int numero2 = Integer.parseInt(n2);
        int resultado = numero1 - numero2;
        return (resultado);
    }

    public int multiplicarDosEnteros(String n1, String n2) {
        int numero1 = Integer.parseInt(n1);
        int numero2 = Integer.parseInt(n2);
        int resultado = numero1 * numero2;
        return (resultado);
    }

    public int dividirDosEnteros(String n1, String n2) {
        int numero1 = Integer.parseInt(n1);
        int numero2 = Integer.parseInt(n2);
        int resultado = numero1 / numero2;
        return (resultado);
    }

}
